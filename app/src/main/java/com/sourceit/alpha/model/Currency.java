package com.sourceit.alpha.model;


import android.os.Parcel;
import android.os.Parcelable;

import java.util.Map;

public class Currency implements Parcelable{
    public String base;
    public String fullName;
    public int description;
    public int image;
    public String date;
    public Map<String, Float> rates;

    public Currency(String base, String fullName, int description, int image) {
        this.base = base;
        this.fullName = fullName;
        this.description = description;
        this.image = image;
    }

    private Currency(Parcel in) {
        base = in.readString();
        fullName = in.readString();
        description = in.readInt();
        image = in.readInt();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(base);
        dest.writeString(fullName);
        dest.writeInt(description);
        dest.writeInt(image);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Currency> CREATOR = new Creator<Currency>() {
        @Override
        public Currency createFromParcel(Parcel in) {
            return new Currency(in);
        }

        @Override
        public Currency[] newArray(int size) {
            return new Currency[size];
        }
    };
}
