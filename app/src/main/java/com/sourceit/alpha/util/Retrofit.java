package com.sourceit.alpha.util;

import com.sourceit.alpha.model.Currency;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;


public class Retrofit {

    private static final String ENDPOINT = "https://api.fixer.io";
    private static ApiInterface apiInterface;

    static {
        initialize();
    }

    interface ApiInterface {
        @GET("/latest")
        void getCurrency(@Query("base") String base, Callback<Currency> callback);

        @GET("/{date}")
        void getByDate(@Path("date") String date, Callback<Currency> callback);
    }

    private static void initialize() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(ENDPOINT)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();
        apiInterface = restAdapter.create(ApiInterface.class);
    }

    public static void getCurrency(String base, Callback<Currency> callback) {
        apiInterface.getCurrency(base, callback);
    }

    public static void getByDate(String date, Callback<Currency> callback) {
        apiInterface.getByDate(date, callback);
    }
}


